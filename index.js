import { AppRegistry } from 'react-native';
import App from './App.js';
import Tfapp from './Tfapp.js';
import TFCamera from './TFCamera';
import AppCV from './AppCV';
AppRegistry.registerComponent('arviro', () => App);

// The below line is necessary for use with the TestBed App
AppRegistry.registerComponent('ViroSample', () => App);
