import { AppRegistry } from 'react-native';
import App from './App.js';
import AppCV from './AppCV';
import Tfapp from './Tfapp'
import Appcoreml from './Appcoreml'
AppRegistry.registerComponent('arviro', () => Appcoreml);

// The below line is necessary for use with the TestBed App
AppRegistry.registerComponent('ViroSample', () => App);