export function random(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}
export function distanceArray3D(arrA, arrB) {
  const a = convertTo3DPoint(arrA);
  const b = convertTo3DPoint(arrB);
  return distance3D(a, b);
}
export function distance3D(a, b) {
  return Math.sqrt(
    Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2) + Math.pow(a.z - b.z, 2)
  );
}
export function convertTo3DPoint(arr) {
  return { x: arr[0], y: arr[1], z: arr[2] };
}
