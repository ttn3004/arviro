import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  StyleSheet,
  Image,
  Dimensions,
} from 'react-native';
const ControlBarScreen = (props) => {
  const [text, setText] = useState('coo');

  useEffect(() => {
    console.log('update');
  }, [props.retDict]);
  const _onScreenShot = () => {
    props.onScreenShot();
  };

  return (
    <View style={localStyles.controlBar}>
      <TouchableHighlight
        style={localStyles.buttons}
        underlayColor={'#68a0ff'}
        onPress={_onScreenShot}
      >
        <Text style={localStyles.buttonText}>screen shot</Text>
      </TouchableHighlight>
      {props.retDict.url && (
        <Image
          style={{ flex: 1, width: 50 }}
          source={{ uri: 'file://' + props.retDict.url }}
        />
      )}
    </View>
  );
};

var localStyles = StyleSheet.create({
  controlBar: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    width: '100%',
    backgroundColor: 'white',
  },
  buttons: {
    height: 80,
    width: 150,
    paddingTop: 20,
    paddingBottom: 20,
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: '#68a0cf',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'center',
  },
});
export default ControlBarScreen;
