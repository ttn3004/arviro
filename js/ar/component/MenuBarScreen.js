import React from 'react';
import { View, Text, TouchableHighlight, StyleSheet } from 'react-native';
const MenuBarScreen = () => {
  return (
    <View style={localStyles.menuBar}>
      <TouchableHighlight style={localStyles.buttons} underlayColor={'#68a0ff'}>
        <Text style={localStyles.buttonText}>Menubar</Text>
      </TouchableHighlight>
      <TouchableHighlight style={localStyles.buttons} underlayColor={'#68a0ff'}>
        <Text style={localStyles.buttonText}>Menubar</Text>
      </TouchableHighlight>
    </View>
  );
};

var localStyles = StyleSheet.create({
  menuBar: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: 'powderblue',
    opacity: 0.5,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  buttons: {
    height: 80,
    width: 150,
    paddingTop: 20,
    paddingBottom: 20,
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: '#68a0cf',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'center',
  },
});
export default MenuBarScreen;
