import React, { useState, useEffect, useRef } from 'react';
import { View, TouchableHighlight, StyleSheet } from 'react-native';
import { ViroARSceneNavigator } from 'react-viro';

import ArSceneBuild from './ArSceneBuild';
import Tflite from 'tflite-react-native';
let tflite = new Tflite();
const height = 350;
const width = 350;

let RNFS = require('react-native-fs');

const ArNavigator = (props) => {
  const ARSceneNav = useRef(null);
  const modelParams = {
    file: 'models/mobilenet_v1_1.0_224_quant.tflite',
    inputDimX: 224,
    inputDimY: 224,
    outputDim: 1001,
    freqms: 0,
  };
  useEffect(() => {
    const modelFile = './../../models/mobilenet_v1_1.0_224.tflite';
    const labelsFile = './../../mobilenet_v1_1.0_224.txt';
    tflite.loadModel(
      {
        model: modelFile,
        labels: labelsFile,
      },
      (err, res) => {
        if (err) console.log(err);
        else console.log('tflite model loaded', res);
      }
    );
  });

  /**check photo */
  _onScreenShot = async () => {
    const begin = new Date().getTime();
    console.log('_onScreenShot', begin);
    const retDict = await ARSceneNav.sceneNavigator.takeScreenshot(
      'picpic',
      false
    );
    // get a list of files and directories in the main bundle
    var path = Platform.OS === 'ios' ? response.url : 'file://' + retDict.url;
    console.log('retDict', retDict, destPath);
    if (retDict.success) {
      tflite.runModelOnImage(
        {
          path,
          imageMean: 128.0,
          imageStd: 128.0,
          numResults: 3,
          threshold: 0.05,
        },
        (err, res) => {
          if (err) console.log(err);
          else console.log('recognitions', res, new Date().getTime() - begin);
        }
      );
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <ViroARSceneNavigator
        ref={ARSceneNav}
        autofocus={true}
        initialScene={{ scene: ArSceneBuild }}
      />
      <TouchableHighlight
        style={localStyles.buttons}
        onPress={() => _onScreenShot}
        underlayColor={'#68a0ff'}
      >
        <Text style={localStyles.buttonText}>screen shot</Text>
      </TouchableHighlight>
    </View>
  );
};

var localStyles = StyleSheet.create({
  buttons: {
    height: 80,
    width: 150,
    paddingTop: 20,
    paddingBottom: 20,
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: '#68a0cf',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
  },
});
export default ArNavigator;
