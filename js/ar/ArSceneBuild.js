import React, { useState, useRef, useEffect } from 'react';
import { View, Text, PermissionsAndroid } from 'react-native';

import {
  ViroARScene,
  ViroText,
  ViroConstants,
  ViroBox,
  ViroMaterials,
  Viro3DObject,
  ViroAmbientLight,
  ViroSpotLight,
  ViroARPlaneSelector,
  ViroNode,
  ViroAnimations,
  ViroButton,
  ViroARPlane,
  ViroPolyline,
  ViroUtils,
  ViroQuad,
} from 'react-viro';
import { random, distanceArray3D } from './../util/NumberUtil';

var objJson = require('./../res/coffee_mug/object_coffee_mug.vrx');
const ArSceneBuild = (props) => {
  const refScene = useRef();
  const arNodeRef = useRef();
  const spotLightRef = useRef();
  const [state, setState] = useState({
    objPosition: [0, 0, 0],
    scale: [0.2, 0.2, 0.2],
    rotation: [0, 0, 0],
    shouldBillboard: true, //toujours afficher en face du caméra
  });
  const [transformBehaviors, setTransformBehaviors] = useState([]);

  useEffect(() => {
    //console.log('Scene - useEffect', JSON.stringify(state));
    if (state.shouldBillboard) {
      setTransformBehaviors(['billboardY']);
    } else {
      setTransformBehaviors([]);
    }
  }, [props.arSceneNavigator.viroAppProps, state]);

  _onTrackingUpdated = (state, reason) => {
    if (state === ViroConstants.TRACKING_NORMAL) {
      //console.log('Scene - _onTrackingUpdated OK');
      props.onTrackingInit();
    } else if (state == ViroConstants.TRACKING_NONE) {
      console.error('reason', reason);
    }
  };

  _onArHitTestResults = (position, forward, results) => {
    // Default position is just 1.5 meters in front of the user.
    let newPosition = [forward[0] * 1.5, forward[1] * 1.5, forward[2] * 1.5];
    let hitResultPosition = undefined;
    //console.log('_onArHitTestResults', position, newPosition);
    // Filter the hit test results based on the position.
    if (results.length > 0) {
      for (var i = 0; i < results.length; i++) {
        let result = results[i];
        if (result.type == 'ExistingPlaneUsingExtent') {
          var distance = Math.sqrt(
            (result.transform.position[0] - position[0]) *
              (result.transform.position[0] - position[0]) +
              (result.transform.position[1] - position[1]) *
                (result.transform.position[1] - position[1]) +
              (result.transform.position[2] - position[2]) *
                (result.transform.position[2] - position[2])
          );
          if (distance > 0.2 && distance < 10) {
            // If we found a plane greater than .2 and less than 10 meters away then choose it!
            hitResultPosition = result.transform.position;
            const dist = distanceArray3D(position, result.transform.position);
            //console.log('ExistingPlaneUsingExtent', distance, dist);
            break;
          }
        } else if (result.type == 'FeaturePoint' && !hitResultPosition) {
          // If we haven't found a plane and this feature point is within range, then we'll use it
          // as the initial display point.
          var distance = _distance(position, result.transform.position);
          const dist = distanceArray3D(position, result.transform.position);
          if (distance > 0.2 && distance < 10) {
            hitResultPosition = result.transform.position;
            //console.log('FeaturePoint', distance, dist, hitResultPosition);
          }
        }
      }
    }

    if (hitResultPosition) {
      newPosition = hitResultPosition;
    }

    // Set the initial placement of the object using new position from the hit test.
    _setInitialPlacement(newPosition);
  };
  // Set the initial placement of the object using new position from the hit test.
  _setInitialPlacement = (position) => {
    setState({
      ...state,
      objPosition: position,
    });

    //Update the rotation of the object to face the user after it's positioned.
    arNodeRef.current.getTransformAsync().then((retDict) => {
      let rotation = retDict.rotation;
      let absX = Math.abs(rotation[0]);
      let absZ = Math.abs(rotation[2]);
      let yRotation = rotation[1];
      // If the X and Z aren't 0, then adjust the y rotation.
      if (absX > 1 && absZ > 1) {
        yRotation = 180 - yRotation;
      }
      //console.log('Scene - _updateInitialRotation', JSON.stringify(state));
      setState({
        ...state,
        rotation: [0, yRotation, 0],
        shouldBillboard: false,
        objPosition: position,
      });
    });
  };
  // Update the rotation of the object to face the user after it's positioned.
  const _updateInitialRotation = () => {
    arNodeRef.current.getTransformAsync().then((retDict) => {
      let rotation = retDict.rotation;
      let absX = Math.abs(rotation[0]);
      let absZ = Math.abs(rotation[2]);

      let yRotation = rotation[1];

      // If the X and Z aren't 0, then adjust the y rotation.
      if (absX > 1 && absZ > 1) {
        yRotation = 180 - yRotation;
      }
      //console.log('Scene - _updateInitialRotation', JSON.stringify(state));
      setState({
        ...state,
        rotation: [0, yRotation, 0],
        shouldBillboard: false,
      });
    });
  };
  // Calculate distance between two vectors
  _distance = (vectorOne, vectorTwo) => {
    var distance = Math.sqrt(
      (vectorTwo[0] - vectorOne[0]) * (vectorTwo[0] - vectorOne[0]) +
        (vectorTwo[1] - vectorOne[1]) * (vectorTwo[1] - vectorOne[1]) +
        (vectorTwo[2] - vectorOne[2]) * (vectorTwo[2] - vectorOne[2])
    );
    return distance;
  };
  _onLoadEnd = () => {
    console.log('Scene - _onLoadEnd');
    refScene.current.getCameraOrientationAsync().then((orientation) => {
      refScene.current
        .performARHitTestWithRay(orientation.forward)
        .then((results) => {
          _onArHitTestResults(
            orientation.position,
            orientation.forward,
            results
          );
        });
    });
    props.onLoadEnd();
  };
  _onLoadStart = () => {
    //console.log('Scene - _onLoadStart');
    setState({
      ...state,
      objPosition: [0, 0, 0],
      shouldBillboard: true,
    });
    props.onLoadStart();
  };
  /*
   Rotation should be relative to its current rotation *not* set to the absolute
   value of the given rotationFactor.
   */
  _onRotate = (rotateState, rotationFactor, source) => {
    console.log('Scene - _onRotate', rotateState, rotationFactor, source);
    if (rotateState == 3) {
      setState({
        ...state,
        rotation: [
          state.rotation[0],
          state.rotation[1] + rotationFactor,
          state.rotation[2],
        ],
      });
      return;
    }
    arNodeRef.current.setNativeProps({
      rotation: [
        state.rotation[0],
        state.rotation[1] + rotationFactor,
        state.rotation[2],
      ],
    });
  };
  /*
   Pinch scaling should be relative to its last value *not* the absolute value of the
   scale factor. So while the pinching is ongoing set scale through setNativeProps
   and multiply the state by that factor. At the end of a pinch event, set the state
   to the final value and store it in state.
   */
  _onPinch = (pinchState, scaleFactor, source) => {
    console.log('Scene - _onPinch', pinchState, scaleFactor, source);
    var newScale = state.scale.map((x) => {
      return x * scaleFactor;
    });

    if (pinchState == 3) {
      setState({
        ...state,
        scale: newScale,
      });
      return;
    }

    arNodeRef.current.setNativeProps({ scale: newScale });
    spotLightRef.current.setNativeProps({ shadowFarZ: 6 * newScale[0] });
  };
  _onCameraTransformUpdate = (result) => {
    // console.log('Scene - _onCameraTransformUpdate', result);
  };

  return (
    <ViroARScene
      ref={refScene}
      onTrackingUpdated={_onTrackingUpdated}
      onCameraTransformUpdate={_onCameraTransformUpdate}
    >
      <ViroAmbientLight color="#ffffff" intensity={200} />
      {props.arSceneNavigator.viroAppProps.objectSource &&
        props.arSceneNavigator.viroAppProps.displayObject && (
          <ViroNode
            transformBehaviors={transformBehaviors}
            visible={props.arSceneNavigator.viroAppProps.displayObject}
            position={state.objPosition}
            onDrag={() => {}}
            ref={arNodeRef}
            scale={state.scale}
            rotation={state.rotation}
            onRotate={_onRotate}
            dragType="FixedToWorld"
            key={props.arSceneNavigator.viroAppProps.displayObjectName}
          >
            <ViroSpotLight
              innerAngle={5}
              outerAngle={20}
              direction={[0, -1, 0]}
              position={[0, 4, 0]}
              color="#ffffff"
              castsShadow={true}
              shadowNearZ={0.1}
              shadowFarZ={6}
              shadowOpacity={0.9}
              ref={spotLightRef}
            />

            <Viro3DObject
              position={[0, props.arSceneNavigator.viroAppProps.yOffset, 0]}
              source={props.arSceneNavigator.viroAppProps.objectSource}
              type="VRX"
              onLoadEnd={_onLoadEnd}
              onLoadStart={_onLoadStart}
              onPinch={_onPinch}
            />

            <ViroQuad
              rotation={[-90, 0, 0]}
              position={[0, -0.001, 0]}
              width={2.5}
              height={2.5}
              arShadowReceiver={true}
              ignoreEventHandling={true}
            />
          </ViroNode>
        )}
    </ViroARScene>
  );
};
export default ArSceneBuild;
