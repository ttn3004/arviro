/**
 * Copyright (c) 2017-present, Viro, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */
import React, {
  Component,
  useState,
  useRef,
  createRef,
  useEffect,
} from 'react';
import {
  AppRegistry,
  Text,
  View,
  StyleSheet,
  PixelRatio,
  TouchableHighlight,
  Platform,
  Image,
  Alert,
  ActivityIndicator,
} from 'react-native';
import { ViroVRSceneNavigator, ViroARSceneNavigator } from 'react-viro';
import _ from 'lodash';
var RNFS = require('react-native-fs');

import ArSceneBuild from './js/ar/ArSceneBuild';
import ControlBarScreen from './js/ar/component/ControlBarScreen';
import MenuBarScreen from './js/ar/component/MenuBarScreen';
var objArray = [
  require('./js/res/coffee_mug/object_coffee_mug.vrx'),
  require('./js/res/object_flowers/object_flowers.vrx'),
  require('./js/res/emoji_smile/emoji_smile.vrx'),
];

const ViroSample = () => {
  const refArSceneNav = useRef();
  const [tfModel, setTfModel] = useState('');
  const [retDict, setRetDict] = useState({});
  const [trackingInitialized, setTrackingInitialized] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [state, setState] = useState({
    viroAppProps: {
      displayObject: false,
      objectSource: objArray[0],
      yOffset: 0,
    },
  });
  useEffect(() => {
   // console.log('App - useEffect trackingInitialized', state.viroAppProps);
  }, [state]);
  /**BEGIN */
  const _onShowObject = (objIndex, objUniqueName, yOffset) => {
    //console.log('App - _onShowObject', objIndex, objUniqueName, yOffset);
    setState({
      ...state,
      viroAppProps: {
        ...state.viroAppProps,
        displayObject: true,
        yOffset: yOffset,
        displayObjectName: objUniqueName,
        objectSource: objArray[objIndex],
      },
    });
  };
  const _renderTrackingText = () => {
    if (trackingInitialized) {
      return (
        <View
          style={{
            position: 'absolute',
            backgroundColor: '#ffffff22',
            left: 30,
            right: 30,
            top: 30,
            alignItems: 'center',
          }}
        >
          <Text style={{ fontSize: 12, color: '#ffffff' }}>
            Tracking initialized.
          </Text>
        </View>
      );
    } else {
      return (
        <View
          style={{
            position: 'absolute',
            backgroundColor: '#ffffff22',
            left: 30,
            right: 30,
            top: 30,
            alignItems: 'center',
          }}
        >
          <Text style={{ fontSize: 12, color: '#ffffff' }}>
            Waiting for tracking to initialize.
          </Text>
        </View>
      );
    }
  };
  const _onTrackingInit = () => {
    //console.log('App - _onTrackingInit');
    setTrackingInitialized(true);
  };
  const _onDisplayDialog = () => {
    Alert.alert('Choose an object', 'Select an object to place in the world!', [
      {
        text: 'Coffee Mug',
        onPress: () => _onShowObject(0, 'coffee_mug', 0),
      },
      {
        text: 'Flowers',
        onPress: () => _onShowObject(1, 'flowers', 0.29076),
      },
      {
        text: 'Smile Emoji',
        onPress: () => _onShowObject(2, 'smile_emoji', 0.497823),
      },
    ]);
  };
  const _onLoadStart = () => {
    //console.log('App - _onLoadStart');
    setLoading(true);
  };
  const _onLoadEnd = () => {
   // console.log('App - _onLoadEnd');
    setLoading(false);
  };
  /**END */
  const handleOnScreenShot = () => {
    const begin = new Date().getTime();
    console.log('App - _onScreenShot', begin);
    refArSceneNav.current.sceneNavigator
      .takeScreenshot(begin + '_img', false)
      .then((retDict) => {
        console.log('App - retDict', retDict);
        setRetDict(retDict);
      })
      .catch((error) => {
        console.log('Api call error', error);
      });
  };

  return (
    <View style={localStyles.outer}>
      <ViroARSceneNavigator
        style={localStyles.arView}
        autofocus={true}
        initialScene={{
          scene: ArSceneBuild,
          passProps: {
            onTrackingInit: _onTrackingInit,
            onLoadStart: _onLoadStart,
            onLoadEnd: _onLoadEnd,
          },
        }}
        viroAppProps={state.viroAppProps}
        ref={refArSceneNav}
      />
      {_renderTrackingText()}
      {isLoading && (
        <View
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <ActivityIndicator
            size="large"
            animating={isLoading}
            color="#ffffff"
          />
        </View>
      )}
      <View
        style={{
          position: 'absolute',
          left: 0,
          right: 0,
          bottom: 77,
          alignItems: 'center',
        }}
      >
        <TouchableHighlight
          style={localStyles.buttons}
          onPress={_onDisplayDialog}
          underlayColor={'#00000000'}
        >
          <Image source={require('./js/res/btn_mode_objects.png')} />
        </TouchableHighlight>
      </View>
    </View>
  );
};

var localStyles = StyleSheet.create({
  crosshair: {
    position: 'absolute',
    borderRadius: 15,
    borderWidth: 1,
    backgroundColor: 'grey',
  },
  container: {
    flex: 1,
    backgroundColor: 'pink',
  },
  preview: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cameraText: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
  },
  viroContainer: {
    flex: 1,
    backgroundColor: 'black',
  },
  outer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'black',
  },
  inner: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: 'black',
  },
  titleText: {
    paddingTop: 30,
    paddingBottom: 20,
    color: '#fff',
    textAlign: 'center',
    fontSize: 25,
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 20,
  },
  buttonsAr: {
    height: 80,
    width: 150,
    paddingTop: 20,
    paddingBottom: 20,
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: '#68a0cf',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
  },
  exitButton: {
    height: 50,
    width: 100,
    paddingTop: 10,
    paddingBottom: 10,
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: '#68a0cf',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
  },
  outer: {
    flex: 1,
  },

  arView: {
    flex: 1,
  },
  buttons: {
    height: 80,
    width: 80,
    paddingTop: 20,
    paddingBottom: 20,
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: '#00000000',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#ffffff00',
  },
});

export default ViroSample;
module.exports = ViroSample;
