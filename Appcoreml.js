
import React, {
    Component,
    useState,
    useRef,
    createRef,
    useEffect,
  } from 'react';
  import {
    AppRegistry,
    Text,
    View,
    StyleSheet,
    PixelRatio,
    TouchableHighlight,
    Platform,
  } from 'react-native';
  import CoreMLImage from "react-native-core-ml-image";

  const BEST_MATCH_THRESHOLD = 0.5;

  const Appcoreml = () => {
    const[bestMatch,setBestMatch] =useState(null)
    const[classification,setSlassification] =useState(null)

    useEffect(()=>{
        if (bestMatch) {
         // console.log('useEffect',bestMatch)
          if (bestMatch && bestMatch.identifier) {
            setSlassification(bestMatch.identifier);
          } else {
            setSlassification(null);
          }
    
        }
    },[bestMatch,classification])

    const onClassification=(classifications) =>{
        var bestMatch = null;
    
        if (classifications && classifications.length > 0) {
          // Loop through all of the classifications and find the best match
          classifications.forEach((classification) => {
            if (!bestMatch || classification.confidence > bestMatch.confidence) {
              bestMatch = classification;
            }
          });
    
          // Is best match confidence better than our threshold?
          if (bestMatch.confidence >= BEST_MATCH_THRESHOLD) {
            setBestMatch(bestMatch)
         //console.log('************************************* *************************************',bestMatch)
          } else {
            setBestMatch(null)
          }
    
        } else {
            setBestMatch(null)
        }
        
      }


    return (
        <View style={styles.container}>
        <CoreMLImage modelFile="MobileNetV2" onClassification={(evt) => onClassification(evt)}>
            <View style={styles.container}>
              <Text style={styles.info}>{classification}</Text>
            </View>
        </CoreMLImage>
    </View>
    );
  };
  
const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'transparent',
    },
    info: {
      fontSize: 20,
      color: "#ffffff",
      textAlign: 'center',
      fontWeight: "900",
      margin: 10,
    }
  });
  export default Appcoreml;
  